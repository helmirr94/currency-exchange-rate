@echo off


:start
cls

set python_ver=36

python ./get-pip.py

cd \
cd \python%python_ver%\Scripts\
pip install schedule
pip install fastapi
pip install "uvicorn[standard]"
pip install xmltodict
pip install dicttoxml
pip install pytest
python -m uvicorn main:app --reload