import schedule
import xmltodict
import requests
from dicttoxml import dicttoxml
from dotenv import load_dotenv
import os
from datetime import datetime as dt

load_dotenv()
api_key = os.getenv('API_KEY_EXCHANGE_RATE')

currencies = dict(
    EUR=f'https://v6.exchangerate-api.com/v6/{api_key}/latest/EUR',
    USD=f'https://v6.exchangerate-api.com/v6/{api_key}/latest/USD',
    GBP=f'https://v6.exchangerate-api.com/v6/{api_key}/latest/GBP',
    CHF=f'https://v6.exchangerate-api.com/v6/{api_key}/latest/CHF'
)


changes = dict()#global variable to save currency changes 
def track_changes(new_data: dict, filename: str):
    t = str(dt.now())#get current time 
    old_data = None
    with open(filename, "rb") as f:
        old_data = xmltodict.parse(f.read()).get('root')
    
    diff = {}
    for currency, exchange in new_data.items():#loop throught currencies(EUR,USD,GBP,CHF)
        temp = dict((k, float(v) - float(old_data[currency][k])) for k, v in exchange.items() if float(v) != float(old_data[currency][k]))#calculate the differences with the old data fetched two hours ago
        for k,v in temp.items():
            diff[f"{currency}/{k}"] = v
    if diff:
        changes[t] = diff
    print(changes)    


def fetch_currency():
    ''' fetch currency from api every two hours or when triggert throught the api '''
    data = dict()
    for id_, value in currencies.items():   
        print(f"fetching {id_}") #print the currency to fetch 
        data[id_] = dict((key , requests.get(value).json().get('conversion_rates').get(key)) for key in currencies.keys()) #get only needed currencies(EUR,USD,GBP,CHF)
    
    xml = dicttoxml(data, attr_type=False) #convert data to xml 
    #write data into file "exchange_rate"
    with open("exchange_rate.xml", "wb") as f:
        f.write(xml)
    track_changes(data, "exchange_rate.xml")
    return data    

def daily_report():
    with open("daily_report.xml", "wb") as f:
        f.write(dicttoxml(changes))
    changes = {} #reset changes


#run scheduler every 2 hours 
if __name__ == "__main__":   
    schedule.every().day.at("00:00").do(daily_report)
    schedule.every(2).hours.do(fetch_currency)
    #schedule.every(5).seconds.do(fetch_currency)

    while True:
        schedule.run_pending()
