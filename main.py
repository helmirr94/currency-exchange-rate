from fastapi import FastAPI, Path, HTTPException
import requests
from dotenv import load_dotenv
import os
from fetch_job import fetch_currency

load_dotenv()
api_key = os.getenv('API_KEY_EXCHANGE_RATE') #get api-key from .env file

app = FastAPI() #init fast api

currency_description = "Currency codes (ISO 4217)"


#routes

@app.get("/", summary="home")
async def welcome():
    return {"msg": "Welcome to currency exchange rate app"}

    
@app.get("/read-exchange-rate/{currency1}/{currency2}", summary="get exchange rate between two currencies. The currency codes must meet the following requirements (ISO 4217)")
def read_exchange_rate(
    currency1 : str = Path(description=currency_description, max_length=3),
    currency2 : str = Path(description=currency_description, max_length=3)):
    exchange_url = f'https://v6.exchangerate-api.com/v6/{api_key}/pair/{currency1}/{currency2}'
    response = requests.get(exchange_url).json()
    if response.get('result') == 'error':
        raise HTTPException(status_code=400, detail="Bad request")
    return response

@app.get("/read-report-data/{currency}", summary="get report data of currency. The currency codes must meet the following requirements (ISO 4217)")
def read_report_rate(currency : str = Path(description=currency_description, max_length=3)):
    report_url = f'https://v6.exchangerate-api.com/v6/{api_key}/latest/{currency}'
    response = requests.get(report_url).json()
    if response.get('result') == 'error':
        raise HTTPException(status_code=400, detail="Bad request")
    return response

@app.get("/trigger-fetching-process", summary="trigger fetching process once")
def trigger_fetching_process():
    return fetch_currency()
