from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_welcome():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Welcome to currency exchange rate app"}



# test currency codes (ISO 4217)
def test_read_exchange_rate_validation_error():
    response = client.get("/read-exchange-rate/USD/EURO")
    assert response.status_code == 422

# test currency codes (ISO 4217)
def test_read_report_data_validation_error():
    response = client.get("/read-report-data/EURO")
    assert response.status_code == 422    


def test_read_report_data_successfully():
    response = client.get("/read-report-data/EUR")
    assert response.status_code == 200   

def test_read_exchange_rate_successfully():
    response = client.get("/read-exchange-rate/USD/EUR")
    assert response.status_code == 200    

def test_read_report_data_not_found():
    response = client.get("/read-report-data/KKK/KKK")
    assert response.status_code == 404     

def test_read_report_data_with_wrong_param():
    response = client.get("/read-report-data/KKK")
    assert response.status_code == 400  

def test_read_exchange_rate_with_wrong_params():
    response = client.get("/read-exchange-rate/USD/KKK")
    assert response.status_code == 400             