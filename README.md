# currency exchange rate

This is a simple project written in python to fetch currency exchange rate from https://www.exchangerate-api.com/


## install
### Requirement
> python3

> pip

### Dependencies
> pip install schedule

> pip install fastapi

> pip install "uvicorn[standard]"

> pip install xmltodict

> pip install dicttoxml

> pip install dotenv

> pip install pytest


### Starting the project 
go to the project directory
> cd currency-exchange-rate

#### Installation  
1. on windows run the script
> install.bat 

2. on linux run the script
> install.sh


#### Manual installation
1. install all the dependencies
2. start the local server by running the command in project directory

> uvicorn main:app --reload


### API documentation
In browser open the url http://127.0.0.1:8000/docs to see the documentation and test the api.

You can add your api-key for the exchangerate-api in **.env** file.

The given api-key should work fine.

### Running scheduler 
The scheduler to fetch currencies exchange rate is a python script

> python3 fetch_job.py

The scheduler is already configured to fetch data every 2 hours and create a daily report if there is changes in exchange rates.


### Tesing
The tests api can be run with the command in the project directory

>pytest


### Insomnia collection 

You can download insomnia from https://insomnia.rest/download and import the file *Insomnia_2022-10-22.json* to get the requests collection.
